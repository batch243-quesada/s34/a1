const express = require('express');
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended : true}));

// number 1 - 2
app.get("/home", (request, response) => {
	response.send('Welcome to the home page.')
});

// number 3 - 4
let users = [
	{
		username: "Mario",
		password: "luigi123"
	},
	{
		username: "Luigi",
		password: "mario123"
	}
];

app.get("/users", (request, response) => {
	response.send(users);
});


// number 5
app.delete("/delete-user", (request, response) => {
  let message = "";
  if (users.length !== 0) {
    for (let i = 0; i < users.length; i++) {
      if (request.body.username === users[i].username) {
        message = `${request.body.username} has been deleted!`;
        users.splice(i, 1);
        break;
      } else {
      	request.body.username === ""
      	  ? (message = "Please input username.")
      	  : (message = "User does not exist.");
      }
    }
    response.send(message);
  } else {
  	response.send("No users available.");
  };
});

app.listen(port, () => console.log(`Server running at port ${port}.`));